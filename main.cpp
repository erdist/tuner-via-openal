#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <list>
#include "al.h"
#include "alc.h"
#include "kiss_fft.h"
#include <math.h>
#include <time.h>
#include <assert.h>
#include <vector>
#include <string>
#include <cmath>




#define sRate 24000
#define format AL_FORMAT_MONO16
#define captureBufferSize 32768
#define MAX 8
#define PI 3.14159265358979323846
#define harmonics 6
#define minIndex 23
#define maxIndex 8000

using namespace std;

ALCint sample=0;
short captureBuffer[captureBufferSize*2];
list<ALuint> bufferQueue;
ALuint buffers[MAX];
ALuint source[1];
ALint availBuffers=0;
ALuint myBuff;
ALuint buffHolder[MAX];

typedef
struct Complex
{
	double r[captureBufferSize];
	double i[captureBufferSize];
}Complex;

Complex arr;

//void fftr(Complex a)
//{
//	int n = captureBufferSize;
//	double norm = sqrt(1.0 / n);
//
//	    for (int i = 0, j = 0; i < n; i++)
//	    {
//	    	if (j >= i)
//		{
//		    double tr = a.r[j] * norm;
//
//		    a.r[j] = a.r[i] * norm;
//		    a.i[j] = 0.0;
//
//		    a.r[i] = tr;
//		    a.i[i] = 0.0;
//		}
//
//		int m = n / 2;
//		while (m >= 1 && j >= m)
//		{
//		    j -= m;
//		    m /= 2;
//		}
//		j += m;
//	    }
//
//	    for (int mmax = 1, istep = 2 * mmax; mmax < n;
//		 mmax = istep, istep = 2 * mmax)
//	    {
//		double delta = (PI / mmax);
//		for (int m = 0; m < mmax; m++)
//		{
//		    double w = m * delta;
//		    double wr = cos(w);
//		    double wi = sin(w);
//
//		    for (int i = m; i < n; i += istep)
//		    {
//			int j = i + mmax;
//			double tr = wr * a.r[j] - wi * a.i[j];
//			double ti = wr * a.i[j] + wi * a.r[j];
//			a.r[j] = a.r[i] - tr;
//			a.i[j] = a.i[i] - ti;
//			a.r[i] += tr;
//			a.i[i] += ti;
//		    }
//		}
//	    }
//	}

void buildWindow(float *window,short *buffer) {
	int bufferSize = captureBufferSize;
	for (int i = 0; i<captureBufferSize; i++)
	{
		float a = (float)(0.42659 - 0.49656*(cos(2 * PI*i / (bufferSize / 2 - 1))) + 0.076849*(cos(4 * PI*i / (bufferSize / 2 - 1))));

		window[i] = (float)round(buffer[i] * a);

	}

}


int hps(float *spectrum) {
	// set the maximum index to search for a pitch
	int i, j, maxHIndex;
	maxHIndex = captureBufferSize / harmonics;
	if (maxIndex < maxHIndex) {
		maxHIndex = maxIndex;
	}

	// generate the Harmonic Product Spectrum values and keep track of the
	// maximum amplitude value to assign to a pitch.
	float res;
	int maxLocation = minIndex;
	for (j = minIndex; j <= maxHIndex; j++) {
		for (i = 1; i <= harmonics; i++) {  // i=1: double the fundamental
			res = (float)(spectrum[j] * spectrum[j * i]);
			spectrum[j] = res;
		}
		if (spectrum[j] > spectrum[maxLocation]) {
			maxLocation = j;
		}
	}

	// Correct for octave too high errors.  If the maximum subharmonic
	// of the measured maximum is approximately 1/2 of the maximum
	// measured frequency, AND if the ratio of the sub-harmonic
	// to the total maximum is greater than 0.2, THEN the pitch value
	// is assigned to the subharmonic.

	int max2 = minIndex;
	int maxsearch = maxLocation * 3 / 4;
	for (i = minIndex + 1; i<maxsearch; i++) {
		if (spectrum[i] > spectrum[max2]) {
			max2 = i;
		}
	}
	if (abs(max2 * 2 - maxLocation) < 4) {
		if (spectrum[max2] / spectrum[maxLocation] > 0.25) {
			maxLocation = max2;
		}
	}

	return maxLocation;
}


float interpolation(int peakValue,float *spectrum) {
	float addPeak;

	addPeak = (float)(0.5*((spectrum[peakValue - 1] - spectrum[peakValue + 1]) / (spectrum[peakValue - 1] - 2 * spectrum[peakValue] + spectrum[peakValue + 1])));
	return addPeak;
}

void toMusicalNotation(float measuredFrequency) {
	double linear;
	linear = 69.0 + 12.0 * log((double)(measuredFrequency / (float)440)) / log(2);
	linear = fmax(linear, 0.0);
	vector<string> note; 
	string as[]= { "C","C#","D","D#","E","F","F#","G","G#","A","A#","B" };
	note.assign(as, as + 12);
	int x = (int)round(linear);

	int cent =int( 100 * (x - linear));

	int index = x % 12;


	 int octave = (int)fmax(x / 12 - 1, 0);

	cout << note[index] << octave <<" "<<cent<< endl;

	
}

void kiss(short *buffer)
{
	//FILE *fp;
	clock_t time;
	kiss_fft_cfg fft = kiss_fft_alloc(captureBufferSize ,0 ,NULL,NULL);
	kiss_fft_cpx fft_in[captureBufferSize];
	kiss_fft_cpx fft_out[captureBufferSize];
	float window[captureBufferSize];
	float max = 0;
	int i;
	int index = -1;
	float freq;
	float res[captureBufferSize/2+1];
	int maxPeak;
	float addPeak;

	buildWindow(window,buffer);

	for (int i = 0; i < captureBufferSize; i++)
	{
	     fft_in[i].r = window[i];
	     fft_in[i].i = window[i];
	     fft_out[i].r = 0;
	     fft_out[i].i = 0;
	}
	time=clock();
	kiss_fft(fft,fft_in, fft_out);
	free(fft);
	for(i=0;i<captureBufferSize/2+1;i++)
	{
		res[i] = fft_out[i].r*fft_out[i].r + fft_out[i].i*fft_out[i].i;
		res[i] = sqrtf(res[i]);
	}

	maxPeak = hps(res);
	addPeak = interpolation(maxPeak, res);
	//for(i=0;i<captureBufferSize/2+1;i++)
	//{
	//	if(res[i]>max)
	//	{
	//		max=res[i];
	//		index = i;
	//	}
	//}
	float ss = sRate;
	float sd = captureBufferSize;
	freq = ((float)maxPeak + addPeak)*(ss/sd);
	time = clock() - time;
	toMusicalNotation(freq);
	//cout<<freq<<endl;
	////cout<<((float)time/CLOCKS_PER_SEC)<<endl;
	//fp=fopen("Out.txt","a");
	//printf("%.2f\n",freq);
	/*for(int j=0;j<10;j++)
	{
		float dB = 20*log10f(res[i-5+j]);
		cout<<dB<<endl;
	}*/
//	fprintf(fp,"\n");
	//fclose(fp);
}


bool callBackInput(ALCdevice *captureDevice)
{
	alcGetIntegerv(captureDevice,ALC_CAPTURE_SAMPLES,1,&sample);
	if(sample>captureBufferSize)
	{
		alcCaptureSamples(captureDevice,captureBuffer,captureBufferSize);
		return true;
	}
	return false;
}

void callBackOutput()
{
    myBuff = bufferQueue.front();
    bufferQueue.pop_front();
    alBufferData(myBuff,format,captureBuffer,captureBufferSize*sizeof(short),sRate);


    // Queue the buffer
    alSourceQueueBuffers(source[0],1,&myBuff);

    // Restart the source if needed
    // (if we take too long and the queue dries up,
    //  the source stops playing).
    ALint sState=0;
    alGetSourcei(source[0],AL_SOURCE_STATE,&sState);
    if (sState!=AL_PLAYING)
    {
    	alSourcePlay(source[0]);
    }

}
void recoverBuffers()
{
	int i;
	alGetSourcei(source[0],AL_BUFFERS_PROCESSED,&availBuffers);
	if(availBuffers>0)
	{
		alSourceUnqueueBuffers(source[0],availBuffers,buffHolder);
		for(i=0;i<availBuffers;i++)
		{
			bufferQueue.push_back(buffHolder[i]);
		}
	}
}
int main()
{
	bool state;
    bool done = false;
	ALCdevice *device;
	ALCcontext *context;
	device = alcOpenDevice(NULL);
	context = alcCreateContext(device,NULL);
	alcMakeContextCurrent(context);
	ALCdevice *captureDevice;
	captureDevice = alcCaptureOpenDevice(NULL,sRate,format,captureBufferSize*2);
	alcCaptureStart(captureDevice);

	alGenBuffers(MAX,&buffers[0]);

	for (int ii=0;ii<MAX;++ii)
	{
		bufferQueue.push_back(buffers[ii]);
	}

	alGenSources(1,&source[0]);

	while(!done)
	{
		recoverBuffers();
		state = callBackInput(captureDevice);
	    //for(int i=0;i<captureBufferSize;i++)
	    //{
	    //	cout<<captureBuffer[i]<<endl;
	    //}
		kiss(captureBuffer);
		if(state)
		{
			if (!bufferQueue.empty())
			{
				//callBackOutput();
			}
		}
	}

    // Stop capture
    alcCaptureStop(captureDevice);
    alcCaptureCloseDevice(captureDevice);

    // Stop the sources
    alSourceStopv(1,&source[0]);
    for (int ii=0;ii<1;++ii) {
        alSourcei(source[ii],AL_BUFFER,0);
    }
    // Clean-up
    alDeleteSources(1,&source[0]);
    alDeleteBuffers(16,&buffers[0]);
    alcMakeContextCurrent(NULL);
    alcDestroyContext(context);
    alcCloseDevice(device);

    return 0;

}



